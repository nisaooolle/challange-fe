import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/register.css";
const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:3006/user/sign-in",
        {
          email: email,
          password: password,
          username: username
        }
      );
      // jika respon status 200 / ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasill!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data);
        history.push("/");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  // const login = async (e) => {
  //   e.preventDefault();

  //   try {
  //     const { status } = await axios.post(
  //       "http://localhost:3006/user/sign-in",
  //       {
  //         email: email,
  //         password: password,
  //       }
  //     );
  //     //jika repon status 200/ok
  //     if (status === 200) {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Login Berhasil !!!",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       localStorage.setItem("id",data.data);
  //       history.push("/");
  //       //Untuk mereload
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 1500);
  //     }
  //   } catch (error) {
  //     Swal.fire({
  //       icon: "error",
  //       title: "Username atau password tidak valid",
  //       showConfirmButton: false,
  //       timer: 1500,
  //     });
  //     console.log(error);
  //   }
  // };

  return (
    <body className="body1 md:text-base lg:h-100vh sm:text-sm">
      <div className="container1">
        <h3>Login</h3>
        <form className="object-cover" onSubmit={login} method="POST">
          <label>Username</label>
          <br />
          <input
            type="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <br />
          <label>Email</label>
          <br />
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <br />
          <label>Password</label>
          <br />
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <br />
          <button type="submit">Log in</button>
          <p>
            {" "}
            Belum punya akun?
            <a href="/register">Register</a>
          </p>
        </form>
      </div>
    </body>
  );
};

export default Login;
